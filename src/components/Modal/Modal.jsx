import React from 'react';
import Button from "../Button/Button";
import "./Modal.scss"

const Modal = ({children}) => {
    return(
        <div className="modal-overlay">
            <div className='modal'>{children}</div>
        </div>
    )
}

const ModalBody = ({children}) => {
    return (
        <div className="modal-body">{children}</div>
    )
}

const ModalClose = ({onClick}) => {
    return (
        <Button className="modal-close" onClick={onClick}>X</Button>
    )
}

const ModalFooter = ({firstText, secondaryText, firstClick, secondaryClick, buttonOneTitle, buttonTwoTitle, children}) => {
    console.log(buttonOneTitle)
    return (
        <div className='modal-footer'>
            <p className='first-text'>{firstText}</p>
            <p className='secondary-text'>{secondaryText}</p>

            {children}

            <div className='modal-buttons'>
                {firstText && <Button onClick={firstClick} className="cancel-btn">{buttonOneTitle}</Button>}
                {secondaryText && <Button onClick={secondaryClick} className="delete-btn">{buttonTwoTitle}</Button>}
            </div>
        </div>
    )
}

const ModalHeader = ({children}) => {
    return (
        <div className="modal-header">{children}</div>
    )
}


const ModalWrapper = ({onClickOutside, children}) => {

    const handleWrapperClick = (event) => {
        if (event.target.classList.contains('modal-wrapper')) {
            onClickOutside();
        }
    };

    return (
        <div className="modal-wrapper" onClick={handleWrapperClick}>{children}</div>
    )
}

export {Modal, ModalBody, ModalClose, ModalFooter, ModalHeader, ModalWrapper};