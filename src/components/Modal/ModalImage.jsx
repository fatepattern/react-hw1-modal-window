import React from 'react';
import {Modal, ModalBody, ModalClose, ModalFooter, ModalHeader, ModalWrapper} from "./Modal"
import "./ModalImage.scss"


const ModalImage = ({img, content, onClose, alt, firstText, secondaryText, buttonOneTitle, buttonTwoTitle}) => {
    return (
        <ModalWrapper onClickOutside={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                    <img className="modal-img" src={img} alt={alt} />
                    <p>{content}</p>
                </ModalBody>
                <ModalFooter firstClick={onClose} firstText={firstText} secondaryText={secondaryText} buttonOneTitle={buttonOneTitle} buttonTwoTitle={buttonTwoTitle}>

                </ModalFooter>
            </Modal>
        </ModalWrapper>
    )
}

export default ModalImage;