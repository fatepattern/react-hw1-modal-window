import { useState } from 'react'
import './App.css'
import Button from './components/Button/Button'
import ModalImage from './components/Modal/ModalImage';
import ModalText from './components/Modal/ModalText';

function App() {

  const [firstModalOpen, setFirstModal] = useState(false);
  const [secondModalOpen, setSecondModal] = useState(false);

  const openFirstModal = () => {
    setFirstModal(true);
    setSecondModal(false);
  }

  const openSecondModal = () => {
    setSecondModal(true);
    setFirstModal(false);
  }

  const closeFirstModal = () => {
    setFirstModal(false);
  }

  const closeSecondModal = () => {
    setSecondModal(false);
  }

  return (
    <>
      <Button onClick={openFirstModal}>Open first modal</Button>
      <Button onClick={openSecondModal}>Open second modal</Button>

      {firstModalOpen && (
        <ModalImage
          img = "../public/vite.svg"
          firstText = "Product Delete!"
          secondaryText = "By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
          buttonOneTitle = "No, Cancel"
          buttonTwoTitle = "Yes, delete"
          onClose={closeFirstModal}
          alt = "image"
        />
      )}

      {secondModalOpen && (
        <ModalText
          firstText = "Add Product"
          description = "Description for you product"
          buttonOneTitle = "ADD TO FAVORITE"
          onClose={closeSecondModal}
          alt = "image"
        />
      )}

    </>
  )
}

export default App